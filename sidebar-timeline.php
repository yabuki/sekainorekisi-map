<?php
/**
 * The right sidebar containing the main widget area.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<!-- sidebar-timeline start -->
<?php dynamic_sidebar( 'timeline-sidebar' ); ?>
