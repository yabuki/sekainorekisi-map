<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      http://www.designsandcode.com/
 * @copyright 2015 Designs & Code
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */
?>

<?php if ( $query->have_posts() )
{
  ?>
 
<div class="sf-pagenation mb-4">
  <?php next_posts_link( '<i class="fa fa-arrow-circle-left fa-2x" aria-hidden="true"></i>', $query->max_num_pages ); ?><?php previous_posts_link( '<i class="fa fa-arrow-circle-right fa-2x" aria-hidden="true"></i>'); ?>
  <span class="px-1">検索結果 <?php echo $query->found_posts; ?> 件 <span class="text-secondary">Page <?php echo $query->query['paged']; ?> / <?php echo $query->max_num_pages; ?></span></span>
</div>
  
  <?php
  while ($query->have_posts())
  {
    $query->the_post();
    
    ?>
    <div class="line-serch-results">
    <div class="row">
     <div class="col-4">
      <?php 
        if ( has_post_thumbnail() ) {
          echo '<p>';
          the_post_thumbnail("thumbnail");
          echo '</p>';
        }
      ?>       
     </div>
     <div class="col-8">
      <h2 class=""><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
       <?php the_excerpt(); ?>
     </div>  
    </div>
</div>
    
    <?php
  }
  ?>

<div class="sf-pagenation">
  <?php next_posts_link( '<i class="fa fa-arrow-circle-left fa-2x" aria-hidden="true"></i>', $query->max_num_pages ); ?><?php previous_posts_link( '<i class="fa fa-arrow-circle-right fa-2x" aria-hidden="true"></i>'); ?>
  <span class="px-1">検索結果 <?php echo $query->found_posts; ?> 件 <span class="text-secondary">Page <?php echo $query->query['paged']; ?> / <?php echo $query->max_num_pages; ?></span></span>
</div>
 
  <?php
} else {
  ?>
  <?php get_template_part( 'loop-templates/content', 'none' ); ?>
  <?php
}
?>