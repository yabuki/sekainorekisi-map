<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php wp_nav_menu( array( 'theme_location' => 'max_mega_menu_1' ) ); ?>

<div class="wrapper" id="wrapper-footer">

  <div class="<?php echo esc_attr( $container ); ?>">

    <div class="row">

      <div class="col-md-12">

        <footer class="site-footer" id="colophon">

          <div class="site-info">

            <p class="text-center mb-0">Copyright&copy; <?php echo date("Y"); ?> <?php bloginfo( 'name' ); ?> <a href="//www.dmca.com/Protection/Status.aspx?ID=d0a161c6-71ae-4dc1-87d1-998d70b0f58f" title="DMCA.com Protection Status" class="dmca-badge"> <img class="ml-3" src ="https://images.dmca.com/Badges/dmca_protected_sml_120al.png?ID=d0a161c6-71ae-4dc1-87d1-998d70b0f58f"  alt="DMCA.com Protection Status" /></a>  <script src="https://images.dmca.com/Badges/DMCABadgeHelper.min.js"> </script></p>

          </div><!-- .site-info -->

        </footer><!-- #colophon -->

      </div>
      <!--col end -->

    </div><!-- row end -->

  </div><!-- container end -->

</div><!-- wrapper end -->

<!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>
