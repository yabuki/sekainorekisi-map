<?php
/**
 * Template Name: glossary Front page template
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'global-templates/hero' ); ?>

<div class="wrapper" id="f-full-width-page-wrapper">

  <div class="<?php echo esc_attr( $container ); ?>" id="content">

    <div class="row">

      <div class="col-md-12 content-area" id="primary">

        <p class="text-center mb-5 pb-5"><a href="/glossary-index/" class="btn btn-primary btn-lg">歴史用語一覧</a></p><!-- /.center -->

        <?php get_template_part('loop-templates/content', 'front-2'); ?>

      </div><!-- #primary -->
    </div><!-- .row end -->

  </div><!-- Container end -->


</div><!-- Wrapper end -->

<?php get_footer();
