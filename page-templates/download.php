<?php
/**
 * Template Name: Download page template
 *
 * This template can be used to override the default template and sidebar setup
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="download-wrapper">

  <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

    <div class="row">
      <?php get_template_part('parts/breadcrumbs'); ?>

      <?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

      <main class="site-main container" id="main" role="main">
        <?php while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'loop-templates/content', 'download' ); ?>

        <?php endwhile; // end of the loop. ?>

      </main><!-- #main -->

      <?php get_template_part( 'sidebar-templates/sidebar', 'right' ); ?>

    </div><!-- .row -->

  </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer();
