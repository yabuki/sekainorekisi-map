<div class="mt-3 alert alert-warning alert-dismissible fade show" role="alert">
  <strong>ツールチップについて</strong>　歴史用語の緑色のリンクは、デスクトップではマウスオーバーで緑色のツールチップが表示され、クリックすると歴史用語詳細ページが新しいタブに表示されます。タッチデバイスでは、リンクをタップしたままにするとツールチップが表示されます。
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
