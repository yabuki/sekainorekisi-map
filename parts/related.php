<div class="related">
  <p class="mb-0">関連記事</p><!-- /.mb-0 -->

  <?php
  $terms = get_the_terms($post->ID,'glossary_cat');
  foreach( $terms as $term ) {
    $term_slug = $term->slug; // 現在表示している投稿に属しているタームを取得
  }
  $args = array(
  'post_type' => 'glossary', // 投稿タイプのスラッグを指定
  'post__not_in' => array($post->ID), // 現在表示している投稿を除外
  'posts_per_page' => 4, // 表示件数9件
  'orderby' =>  'modified', // ランダム
  'post_status' => 'publish',
  'tax_query' => array( // タクソノミーの指定
    array(
      'taxonomy' => 'glossary_cat',
      'field' => 'slug',
      'terms' => $term_slug, // 取得したタームを指定
    ))
  ); $the_query = new WP_Query($args); if($the_query->have_posts()):
?>
  <div class="row">
    <?php while ($the_query->have_posts()): $the_query->the_post(); ?>

    <div class="col-sm">
      <a href="<?php the_permalink(); ?>">
        <?php if ( has_post_thumbnail() ): ?>
        <?php the_post_thumbnail('thumbnail'); ?>
        <?php else: ?>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/no-image300x300.jpg" alt="<?php the_title(); ?>" />
        <?php endif; ?>
      </a>

      <h3 class="mt-1 mb-3 h6">
        <a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a>
      </h3>
    </div><!-- /.col-sm -->
    <?php endwhile; ?>
  </div><!-- /.row -->
  <?php wp_reset_postdata(); ?>
  <?php else: ?>
  <!-- 投稿が無い場合の処理 -->
  <?php endif; ?>
</div><!-- /.related -->
