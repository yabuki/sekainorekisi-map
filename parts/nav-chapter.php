<?php if(has_term('','chapter')): ?>

<div class="chapter-nav">
  <div class="row">
    <div class="col-6 text-left">

      <?php previous_post_link( '<span class="meta-nav pl-3">Previous Post</span><p class="mb-0 font-weight-bold"><i class="fa fa-chevron-left text-info" aria-hidden="true"></i> %link</p>', '%title', TRUE, ' ', 'chapter' ); ?>
    </div>
    <div class="col-6 text-right">
      <?php next_post_link('<span class="meta-nav pr-3">Next Post</span><p class="text-right mb-0 font-weight-bold">%link <i class="fa fa-chevron-right text-info" aria-hidden="true"></i></p>', '%title' ,TRUE, ' ', 'chapter'); ?>
    </div>
  </div>

  <p class="text-right small"><a href="/%E6%97%A5%E6%9C%AC%E5%8F%B2%E3%83%BB%E4%B8%96%E7%95%8C%E5%8F%B2%EF%BC%88%E7%9B%AE%E6%AC%A1%EF%BC%89/" target="_blank" class="text-dark">日本史・世界史 一覧を見る <i class="fa fa-angle-right" aria-hidden="true"></i></a>
  </p>
</div>
<?php endif; ?>
