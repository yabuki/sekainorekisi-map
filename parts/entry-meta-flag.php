<div class="meta-flag d-inline-flex flex-wrap">
  <?php 
  $terms = wp_get_object_terms($post->ID,'country'); 
  foreach($terms as $term){ 
    echo '<a title=" '.esc_html( $term->name ).'" href="' . get_term_link( $term->slug, 'country' ) . '">' ;
    echo '<i class="flag-icon flag-icon-';
    echo $term->slug.''; 
    echo '"></i></a> ';
  }
  ?>
</div>
