<?php
/**
* The template for displaying search results pages.
*
* @package understrap
*/
// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="search-wrapper">
  <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
    <div class="row">
      <?php get_template_part('parts/breadcrumbs'); ?>

      <?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

      <main class="site-main" id="main">
        <?php if ( have_posts() ) : ?>
        <header class="page-header mb-4">
          <h1 class="page-title">
            <?php
printf(
/* translators: %s: query term */
esc_html__( 'Search Results for: %s', 'sekainorekisi-map' ),
'<span>' . get_search_query() . '</span>'
);
?>
          </h1>
        </header><!-- .page-header -->
        <div class="card-columns">
          <?php /* Start the Loop */ ?>
          <?php while ( have_posts() ) : the_post(); ?>

          <?php get_template_part('parts/card','archive'); ?>
          <?php endwhile; ?>
        </div>
        <?php else : ?>
        <?php get_template_part( 'loop-templates/content', 'none' ); ?>
        <?php endif; ?>
      </main><!-- #main -->
      <!-- The pagination component -->
      <?php understrap_pagination(); ?>
      <!-- Do the right sidebar check -->
      <?php get_template_part( 'global-templates/right-sidebar-check' ); ?>
    </div><!-- .row -->
  </div><!-- #content -->
</div><!-- #search-wrapper -->
<?php get_footer();
