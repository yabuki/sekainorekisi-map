<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

  <!-- download -->
  <?php get_template_part('parts/single', 'jumbotron'); ?>
  <?php get_template_part('parts/alert'); ?>
  <div class="mb-2 border-bottom">
    <?php get_template_part('parts/nav', 'postlink'); ?>
  </div>

  <div class="entry-content download-single mb-5">

    <?php the_content(); ?>

    <?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'sekainorekisi-map' ),
			'after'  => '</div>',
		) );
		?>

  </div><!-- .entry-content -->

</article><!-- #post-## -->
