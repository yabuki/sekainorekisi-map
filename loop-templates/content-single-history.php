<?php
/**
 * Partial template for content in single-history.php
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

  <?php get_template_part('parts/single-jumbotron'); ?>
  <?php 
  if(post_custom('wpcf-previous-era1', 'wpcf-next-era1')) {
    get_template_part('parts/nav', 'era');
  }
  ?>
  <?php get_template_part('parts/alert'); ?>
  <div class="mb-2 border-bottom">
    <?php get_template_part('parts/nav', 'postlink'); ?>
  </div>

  <div class="entry-content content-single-history mb-5">

    <?php $parts = get_extended( $post->post_content ); ?>
    <?php 
    if(strpos(get_the_content(),'id="more-')):
    global $more;$more=0;the_content('');
    ?>
    <?php dynamic_sidebar( 'adsense-fullsize' ); ?>
    <div class="d-block d-md-none g-post-widget mb-3">
      <?php get_template_part('parts/widget', 'profile'); ?>
    </div><!-- #d-sm-none# -->

    <?php 
    $more=1;the_content('',true);
    else:the_content();
    endif;
    ?>

    <div class="d-block d-md-none g-post-widget mb-3">
      <?php get_template_part('parts/widget','info'); ?>
    </div>

    <?php
		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'sekainorekisi-map' ),
				'after'  => '</div>',
			)
		);
		?>
    <!-- #wp link # -->

    <footer class="entry-footer">
      <?php get_template_part( 'parts/sanko'); ?>
      <?php edit_post_link( __( 'Edit', 'sekainorekisi-map' ), '<span class="edit-link">', '</span>' ); ?>
    </footer><!-- .entry-footer -->
  </div><!-- .entry-content -->
</article><!-- # article post# -->
