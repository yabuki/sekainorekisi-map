<?php
/**
 * Partial template for content in single-history.php
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

  <?php get_template_part('parts/single-jumbotron'); ?>
  <?php 
  if(post_custom('wpcf-previous-era1', 'wpcf-next-era1')) {
    get_template_part('parts/nav', 'era');
  }
  ?>
  <?php get_template_part('parts/alert'); ?>
  <div class="mb-2 border-bottom">
    <?php get_template_part('parts/nav', 'postlink'); ?>
  </div>

  <div class="entry-content content-single-glossary mb-5">
    <?php 
    if(strpos(get_the_content(),'id="more-')):
    global $more;$more=0;the_content('');
    ?>
    <?php dynamic_sidebar( 'adsense-fullsize' ); ?>
    <div class="d-block d-md-none g-post-widget mb-3">
      <?php get_template_part('parts/widget', 'profile'); ?>
      <?php get_template_part('parts/widget', 'heritage'); ?>
    </div><!-- #d-sm-none# -->

    <?php 
    $more=1;the_content('',true);
    else:the_content();
    endif;
    ?>

    <?php
      if(post_custom('wpcf-contemporaries')) {
        echo '<div class="alert alert-success"><p class="mb-1 text-dark">同時代の人物</p><h4 class="alert-heading pl-0 bg-non">';
        echo post_custom('wpcf-contemporaries');
        echo '</h4><p>';
        echo post_custom('wpcf-contemporaries-info'); 
        echo '</p></div>';        
      }
    ?>
    <div class="d-block d-md-none g-post-widget mb-3">
      <?php get_template_part('parts/widget','info'); ?>
    </div>
    <?php
		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'sekainorekisi-map' ),
				'after'  => '</div>',
			)
		);
		?>
    <!-- #wp link # -->

    <footer class="entry-footer">

      <?php edit_post_link( __( 'Edit', 'sekainorekisi-map' ), '<span class="edit-link">', '</span>' ); ?>

    </footer><!-- .entry-footer -->
  </div><!-- .entry-content -->
</article><!-- # article post# -->
