<?php
/**
 * Partial template for content in frontpage.php
 *
 * @package understrap
 */
// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
  <div class="entry-content row">

    <?php
      $args = array(
        'parent' => 0,
        'hierarchical' => 0,
        'order' => 'ASC'
      );
      $taxonomy_name = 'glossary_cat';
      $taxonomys = get_terms( $taxonomy_name, $args );
      if ( !is_wp_error( $taxonomys ) && count( $taxonomys ) ):
        foreach ( $taxonomys as $taxonomy ):
          $url = get_term_link( $taxonomy->slug, $taxonomy_name );
      ?>
    <section class="home-section col-md-4 py-3">
      <h3 id="<?php echo esc_html($taxonomy->slug); ?>" class="bg-transparent m-0 p-0"><?php echo esc_html($taxonomy->name); ?>
        <p class="small m-0"><i class="fa fa-arrow-right" aria-hidden="true"></i> <a class="text-muted" href="<?php echo esc_url( home_url( '/' ) ); ?>glossary_cat/<?php echo esc_html($taxonomy->slug); ?>/"><?php echo esc_html($taxonomy->name); ?>一覧</a></p><!-- /.small -->
      </h3>
      <!--<div class="card-columns">-->
      <?php
            $args = array(
              'post_type' => 'glossary',
              'posts_per_page' => 2,
              'orderby' => 'modified',
              'order' => 'ASC',
              'post_status' => 'publish',
              'tax_query' => array(
                'relation' => 'AND',
                array(
                  'taxonomy' => 'glossary_cat',
                  'field' => 'slug',
                  'terms' => $taxonomy->slug,
                  'include_children' => true,
                  'operator' => 'IN'
                ),
              )
            );
            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts() ):
              ?>
      <?php
            $term_id = $taxonomy->term_id;
            $children = get_term_children( $term_id, $taxonomy_name );
            if ( $children ):
              ?>
      <?php
            $term_id = $taxonomy->term_id;
            $taxonomy_name = 'glossary_cat';
            $termchildren = get_term_children( $term_id, $taxonomy_name );
            foreach ( $termchildren as $child ) {
              $term = get_term_by( 'id', $child, $taxonomy_name );
              echo '<a href="' . get_term_link( $child, $taxonomy_name ) . '">' . $term->name . '</a>';
            }
            ?>
      <?php else: ?>
      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

      <div class="card border-light bg-light mb-3">
        <?php 
              if ( has_post_thumbnail() ) {
                echo '<a href="';
                the_permalink();
                echo '" title="';
                the_title_attribute();
                echo '">';
                the_post_thumbnail('thumbnail', array( 'class' => 'card-img-top' ));
                echo '</a>';
              } else {
                                echo '<a href="';
                the_permalink();
                echo '" title="';
                the_title_attribute();
                echo '">';
                echo '<img class="card-top-img" src="'.get_stylesheet_directory_uri().'/images/no-image-small-c.jpg" />';
                echo '</a>';             
              }
              ?>
        <div class="card-body p-1">
          <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title('<h4 class="card-title bg-transparent m-0 p-0">', '</h4>'); ?></a>

          <?php 
        //if (has_term('','glossary_cat')) {
        //echo '<i class="fa fa-folder-o" aria-hidden="true"></i> ';
        //the_terms( $post->ID, 'glossary_cat', '', '/' );
        //}
        ?>
        </div><!-- /.card-body -->
        <div class="card-footer bg-transparent border-white p-1 small">
          <?php get_template_part('parts/entry','meta-flag'); ?>
        </div><!-- /.card-footer -->
      </div><!-- .card-pin -->

      <?php endwhile; ?>
      <?php endif; ?>
      <?php endif;
            wp_reset_postdata();
            ?>
      <!--</div> /.card-columns -->


    </section>
    <?php endforeach; endif; wp_reset_postdata(); ?>

  </div><!-- .entry-content -->
  <footer class="entry-footer">
    <?php edit_post_link( __( 'Edit', 'sekainorekisi-map' ), '<span class="edit-link">', '</span>' ); ?>
  </footer><!-- .entry-footer -->
</article><!-- #post-## -->
