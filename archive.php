<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
?>

<?php
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="archive-wrapper">

  <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

    <div class="row">
      <?php get_template_part('parts/breadcrumbs'); ?>

      <!-- Do the left sidebar check -->
      <?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

      <main class="site-main" id="main">

        <?php if ( have_posts() ) : ?>

        <header class="page-header mb-4">
          <?php 
          the_archive_title( '<h1 class="page-title">', '</h1>' );
          the_archive_description( '<div class="taxonomy-description">', '</div>' );
          ?>
        </header><!-- .page-header -->

        <div class="card-columns">

          <?php /* Start the Loop */ ?>
          <?php while ( have_posts() ) : the_post(); ?>

          <?php //get_template_part('loop-templates/content','search'); ?>
          <?php get_template_part('parts/card', 'archive'); ?>

          <?php endwhile; ?>
        </div>
        <?php else : ?>

        <?php get_template_part( 'loop-templates/content', 'none' ); ?>

        <?php endif; ?>

      </main><!-- #main -->

      <!-- The pagination component -->
      <?php understrap_pagination(); ?>

      <!-- Do the right sidebar check -->
      <?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

    </div> <!-- .row -->

  </div><!-- #content -->

</div><!-- #archive-wrapper -->

<?php get_footer();
